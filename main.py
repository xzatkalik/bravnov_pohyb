#!/usr/bin/env python

import math
import random

SIRKA = 200
VYSKA = 200
ZVYRAZNI_BODY = True

def normalne_rozdelenie(mi, sigma):
    u = random.uniform(0,1)
    v = random.uniform(0,1)

    xx = math.sqrt(-2.0 * math.log(u)) * math.cos(2.0 * math.pi * v) 
    
    return (mi + math.sqrt(sigma) * xx)



def pokus(pocet):
    
    
    name = 'trajektorie'+ str(pocet) + '.svg'
    f=open(name, 'w')
    

    f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n\n')
    f.write('<svg version="1.1" \n baseprofile="full"\nxmlns="http://www.w3.org/2000/svg"\nxmlns:xlink="http://www.w3.org/1999/xlink"\n  xmlns:ev="http://www.w3.org/2001/xml-events"\nheight="'+ str(VYSKA) +'px" \nwidth="' + str(SIRKA) + 'px">\n ')
    
    
    fbody=open('body'+str(pocet)+'.svg' ,'w')
    fbody.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n\n')
    fbody.write('<svg version="1.1" \n baseprofile="full"\nxmlns="http://www.w3.org/2000/svg"\nxmlns:xlink="http://www.w3.org/1999/xlink"\n  xmlns:ev="http://www.w3.org/2001/xml-events"\nheight="'+ str(VYSKA) +'px" \nwidth="' + str(SIRKA) + 'px">\n ')



    #vykreslenie osi
    stred = VYSKA/2
    f.write('<polyline points=" 0,' + str(stred) + ' '+ str(SIRKA)+','+str(stred) + '" style="fill:none;stroke:blue;stroke-width:0.5" />\n\n')
    fbody.write('<polyline points=" 0,' + str(stred) + ' '+ str(SIRKA)+','+str(stred) + '" style="fill:none;stroke:blue;stroke-width:0.5" />\n\n')
    f.write('<polyline points=" 0,0 0,'+ str(VYSKA)+' ' + '" style="fill:none;stroke:blue;stroke-width:0.5" />\n\n')
    fbody.write('<polyline points=" '+str(stred)+',0 '+str(stred)+','+ str(VYSKA)+' ' + '" style="fill:none;stroke:blue;stroke-width:0.5" />\n\n')
  
    
    splna=0
    
    for j in range(pocet):
        #y=range(0, SIRKA)
        y = [0] * SIRKA
                   
        y[0] = stred + 0
        for i in range(1, SIRKA):
            y[i] = y[i-1] + normalne_rozdelenie(0,1)
            
        f.write('<polyline points=" ')
        for i in range(SIRKA):
            f.write(' ' + str(i) + ',' + str(y[i]) + ' ')
        
        #pozor na znamienka zaporne cisla su nad casovou osou kladne pod
        if ( y[10] < stred and y[40] > y[10]  ):
            splna = splna + 1
            f.write('" style="fill:none;stroke:red;stroke-width:0.5" />\n\n')
            fbody.write('<circle cx="' + str(y[10]) + '" cy="' + str(y[40]) + '" r="1" stroke="black" stroke-width="0" fill="green" />\n')
        else:
            f.write('" style="fill:none;stroke:black;stroke-width:0.5" />\n\n')
            fbody.write('<circle cx="' + str(y[10]) + '" cy="' + str(y[40]) + '" r="1" stroke="black" stroke-width="0" fill="black" />\n')
        
    f.write('\n</svg>\n')
    fbody.write('\n</svg>\n')
    pp = float(splna) / float(pocet)
    print "Pocet trajektorii je: " + str(pocet) + " pravdepodonost ze trajektoria splna dany jav je: " + str(pp) + "\n"
    
pokus(10)
pokus(100)
pokus(1000)
pokus(10000)

